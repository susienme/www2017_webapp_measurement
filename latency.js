// Copyright 2010 William Malone (www.williammalone.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*jslint browser: true */
/*global G_vmlCanvasManager */

var drawingApp = (function () {

	"use strict";

	var canvas,
		context,
		canvasWidth = window.innerWidth, //1440,
		canvasHeight = window.innerHeight, //2560,

		markerSize = window.innerWidth*0.04,
		offsetLeft = 0,
		// offsetRight = 0,
		offsetTop = 0,
		blackMakerHeight = window.innerWidth*0.2,
		offsetBottom = 0,//2, //142,	//168-26

		colorPurple = "#cb3594",
		colorGreen = "#659b41",
		colorYellow = "#ffcf33",
		colorBrown = "#986928",

		lineColor = "#e5e5e5",
		lineWidth = 5,

		lastX = -1,
		lastY = -1,
		paint = false,
		curColor = colorPurple,
		drawingAreaWidth = window.innerWidth, //1440,
		drawingAreaHeight = window.innerHeight, //2560,

		// Clears the canvas.
		clearCanvas = function () {
			context.clearRect(0, 0, canvasWidth, canvasHeight);
		},

		drawRect = function (x, y, w, h, col) {
			context.beginPath();
			context.rect(x, y, w, h);
			context.closePath();
			context.fillStyle = col;
			context.fill();
		},

		// Redraws the canvas.
		redraw = function () {

			var locX,
				locY,
				radius,
				i,
				selected;

			clearCanvas();

			if (lastX != -1) {
				// console.log("DRAW")
				context.beginPath();
				context.moveTo(0, lastY);
				context.lineTo(canvasWidth, lastY);
				context.strokeStyle = lineColor;
				context.lineWidth = lineWidth;
				context.stroke();
				context.closePath();
				context.restore();
			}

				// red 
				drawRect(canvasWidth - markerSize - offsetLeft, lastY - markerSize/2, markerSize, markerSize, '#ff0000');

				// black top/bottom
				drawRect(0, offsetTop, canvasWidth, blackMakerHeight, '#000000');
				drawRect(0, canvasHeight - offsetBottom - blackMakerHeight, canvasWidth, blackMakerHeight+10, '#000000');

				
				drawRect(0, canvasHeight - 2 - markerSize , markerSize, markerSize, '#ff0000');											// left bottom - red
				drawRect(canvasWidth - markerSize - offsetLeft, canvasHeight - 2 - markerSize , markerSize, markerSize, '#00ff00');		// right bottom - green
				drawRect(0, offsetTop , markerSize, markerSize, '#0000ff');																// left top - blue
				drawRect(canvasWidth - markerSize - offsetLeft, offsetTop, markerSize, markerSize, '#ffffff');							// right top - white

				drawRect( (canvasWidth - markerSize - offsetLeft)*0.9, offsetTop, markerSize, markerSize, '#0000ff');						// right-ish top - blue				
				drawRect( (canvasWidth - markerSize - offsetLeft)*0.9, canvasHeight - 2 - markerSize, markerSize, markerSize, '#0000ff');	// right-ish bottom - blue				
				drawRect( canvasWidth/2 - markerSize*0.5, canvasHeight*0.08, markerSize, markerSize, '#00ff00');	// center top-ish - green
			
		},

		// Add mouse and touch event listeners to the canvas
		createUserEvents = function () {

			var press = function (e) {
				paint = true;
				offsetLeft = this.offsetLeft;
				// offsetRight = canvasWidth - this.offsetLeft - this.offsetWidth;
				offsetTop = this.offsetTop;
				lastX = -1;
				/*lastX = (e.changedTouches ? e.changedTouches[0].pageX : e.pageX) - this.offsetLeft;
				lastY = (e.changedTouches ? e.changedTouches[0].pageY : e.pageY) - this.offsetTop;*/
				// console.log("offset l(" + offsetLeft + ") t(" + offsetTop + ")");
				// console.log("YMH TIMING INPUT-DOWN XY(" + lastX + ", " + lastY + ")");
				// redraw();
			},

			drag = function (e) {
				if (paint) {
					lastX = (e.changedTouches ? e.changedTouches[0].pageX : e.pageX) - this.offsetLeft;
					lastY = (e.changedTouches ? e.changedTouches[0].pageY : e.pageY) - this.offsetTop;
					/*if (e.touches) console.log("YMH MOVE_touches " + e.touches.length + " Y(" + e.changedTouches[0].pageY + ") absY(" + (e.changedTouches[0].pageY + 88 + 196) + ")");
					else console.log("MOVE NULL");*/
					// console.log("YMH TIMING INPUT-MOVE XY(" + lastX + ", " + lastY + ")");
                    // console.log("YMH TIMING wh(" + window.innerWidth + ", " + window.innerHeight + ")" + "wh(" + window.outerWidth + ", " + window.outerHeight + ")");
					redraw();
					// console.log("YMH TIMING INPUT-MOVE processed");
				}
				// Prevent the whole page from dragging if on mobile
				e.preventDefault();
			},

			release = function () {
				paint = false;
				// console.log("YMH TIMING INPUT-UP");
				// redraw();
			},

			cancel = function () {
				paint = false;
			};

			// Add mouse event listeners to canvas element
			canvas.addEventListener("mousedown", press, false);
			canvas.addEventListener("mousemove", drag, false);
			canvas.addEventListener("mouseup", release);
			canvas.addEventListener("mouseout", cancel, false);

			// Add touch event listeners to canvas element
			canvas.addEventListener("touchstart", press, false);
			canvas.addEventListener("touchmove", drag, false);
			canvas.addEventListener("touchend", release, false);
			canvas.addEventListener("touchcancel", cancel, false);
		},

		// Creates a canvas element, loads images, adds events, and draws the canvas for the first time.
		init = function () {
			// console.log("CANVAS sw(" + window.screen.width + ")" );
			// Create the canvas (Neccessary for IE because it doesn't know what a canvas element is)
			canvas = document.createElement('canvas');
			canvas.setAttribute('width', canvasWidth);
			canvas.setAttribute('height', canvasHeight);
			console.log("YMH CANVAS 22 width(" + canvasWidth + ") height(" + canvasHeight + ")");
			canvas.setAttribute('id', 'canvas');
			document.getElementById('canvasDiv').appendChild(canvas);
			if (typeof G_vmlCanvasManager !== "undefined") {
				canvas = G_vmlCanvasManager.initElement(canvas);
			}
			context = canvas.getContext("2d"); // Grab the 2d canvas context
			// Note: The above code is a workaround for IE 8 and lower. Otherwise we could have used:
			//     context = document.getElementById('canvas').getContext("2d");

			createUserEvents();
			redraw()
			// window.scrollTo(0,1);
		};

	return {
		init: init
	};
}());